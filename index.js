const Default = {
  verb: "parse",
  object: "vast"
};

module.exports = (verb = Default.verb, object = Default.object) => {
  if (!verb || typeof verb !== "string") {
    throw new Error("Expected verb as string");
  }

  if (!object || typeof object !== "string") {
    throw new Error("Expected object as string");
  }

  return `ready to ${verb.toUpperCase()} some ${object.toUpperCase()}`;
};
