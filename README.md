# alexify

Alexify your instructions.

[![pipeline status](https://gitlab.com/stevestevesteve/alexify/badges/master/pipeline.svg)](https://gitlab.com/stevestevesteve/alexify/commits/master)

### Install

```bash
$ yarn add alexify
```

### Example

```javascript
// index.js
const http = require('http');
const alexify = require('alexify');

const server = http.createServer((req, res) => {
	res.statusCode = 200;
	res.end('hello world');
});

server.listen(80, '0.0.0.0', () => {
	console.log(alexify('greet', 'users'));
});

```

### Output

```bash
$ node .
ready to GREET some USERS
```

### Home

https://gitlab.com/stevestevesteve/alexify